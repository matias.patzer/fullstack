/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia2;

import java.util.Scanner;

/**
 Escribir un programa que reciba un n�mero entero por teclado. A
continuaci�n, mostrar la tabla de multiplicar de ese n�mero.
 */
public class Ejercicio1 {
    public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    System.out.println("Ingrese el n�mero a formar la tabla: ");
    int num = s.nextInt();
    for (int i=0; i<=10 ; i++){
        System.out.println(num + "x" + i + "= " + i*num);
    }
    
        }
    }
