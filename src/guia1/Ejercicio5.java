/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia1;

import java.util.Scanner;


/**
Pedir al usuario que ingrese el valor del radio de una circunferencia.
Calcular y mostrar por pantalla el �rea y el per�metro. Record� que el
�rea y el per�metro se calculan con las siguientes f�rmulas:
area = PI � radio2
perimetro = 2 � PI � radio
Tip: para este ejercicio se puede usar la constante matem�tica PI ( ) ?
disponible en la clase Math.
 */


public class Ejercicio5 {
    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el radio de la circunferencia: ");
        double radio = s.nextDouble();
        double area = Math.PI*Math.pow(radio,2.0);
        double perimetro = 2*Math.PI*radio;
        System.out.println("Area: " + area + "\n" + "Per�metro: " + perimetro);
        
        
    
    }
}

