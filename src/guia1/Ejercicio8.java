/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia1;

import java.util.Scanner;

// Ejercicio 8
    /*
    Pedir al usuario que ingrese una temperatura en grados Celsius y
    mostrar por pantalla su equivalente en kelvin y grados Fahrenheit. Las
    f�rmulas para conversiones son:
    Kelvin = 273,15 + Celsius
    Fahrenheit = 1,8 � Celsius
    */

public class Ejercicio8 {
    public static void main(String[] args) {
    
        System.out.println("EJERCICIO 8: Conversi�n de temperaturas");
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese la temperatura actual en grados Celsius: ");
        double temp = s.nextDouble();
        double tempK;
        tempK = 273.15 + temp;
        double tempF; 
        tempF=(1.8 * temp);
        System.out.println("En Kelvin= " + tempK + " ; " 
                + "En farenheit= " + tempF);
    
     }
}
