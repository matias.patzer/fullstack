/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia2;

/**
 *
 * @author matia
 */


/*
CONSIGNA
Escribir un algoritmo que imprima el listado de los n�meros primos
menores que 200. Aclaraci�n: el n�mero 1 no es primo.
Tip: un n�mero es primo si es divisible �nicamente por 1 y por s�
mismo.
Por ejemplo, el 7 es primo porque s�lo es divisible por 1 y por 7.
El 6 no es primo porque es divisible por 1, por 2, por 3 y por 6.
*/

public class Ejercicio6 {
    
    public static void main(String[] args) {
        System.out.println("Los numeros primos menores a 200 son: ");

        //boolean esPrimo = false;
        System.out.print("1, 2, 3, ");
        for(int i=2 ; i<200 ; i++){
            if(i%2!=0){
                if (i%3!=0){
                 //esPrimo = true;
                    System.out.print(" " + i + ", ");
                }
            }
           }
        }
        
        
    }
    

