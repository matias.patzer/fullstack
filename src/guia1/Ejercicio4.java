/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia1;

import java.util.Scanner;


/* CONSIGNA
Escribir un programa que lea la estatura de tres personas, calcule el
promedio de la altura de ellos y lo informe.
 */


public class Ejercicio4 {
    public static void main(String[] args) {
        System.out.println("Ejercicio 4: promedio de alturas");
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese altura de la persona 1: ");
        double alt1= s.nextDouble();
        System.out.println("Ingrese altura de la persona 2: ");
        double alt2= s.nextDouble();
        System.out.println("Ingrese altura de la persona 3: ");
        double alt3= s.nextDouble();
        double prom = (alt1+alt2+alt3)/3;
        prom = Math.round(prom);
        
        System.out.println("El promedio de las alturas es de: " + prom + " cm");
        
        
    
    }
}

