/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia1;

import java.util.Scanner;

/**
A partir de una cantidad de pesos que el usuario ingresa a trav�s del
teclado se debe obtener su equivalente en d�lares, en euros, en
guaran�es y en reales. Para este ejercicio se consideran las siguientes
tasas:
1 d�lar = 231,68 pesos
1 euro = 250,69 pesos
1 peso = 31,00 guaran�es
1 real = 46,81 pesos
Tip: en este ejercicio se puede usar la funci�n printf y mostrar el
resultado con dos decimales.
 */
public class Ejercicio9 {
    
     public static void main(String[] args) {
    
        System.out.println("EJERCICIO 9: Conversi�n de divisas");
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese la cantidad en pesos "
                + "que desea convertir: ");
        double pesos = s.nextDouble();
        double dolares,euros,guaranies,reales;
        dolares = pesos/231.68;
        euros = pesos/250.69;
        reales = pesos/46.81;
        guaranies = pesos*31.0;
        
        System.out.println("El monto ingresado en pesos fue de: $" + pesos);
        System.out.print("U$D = ");
        System.out.printf("%.2f",dolares);
        System.out.println("");
        System.out.print("EUROS = ");
        System.out.printf("%.2f",euros);
        System.out.println("");
        System.out.print("R$ = ");
        System.out.printf("%.2f",reales);
        System.out.println("");
        System.out.print("guaranies = ");
        System.out.printf("%.2f",guaranies);
        System.out.println("");
        
     }
    
}
