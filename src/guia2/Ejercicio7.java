/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia2;

import java.util.Scanner;

/** CONSIGNA
 *Del listado de abajo, determinar qu� animal eligi� el usuario mediante
la realizaci�n de tres preguntas del tipo SI/NO acerca de las tres
caracter�sticas elegidas (herb�voro, mam�fero, dom�stico). Mostrar el
resultado por pantalla.

* 
 */
public class Ejercicio7 {
    public static void main(String[] args) {
        String [] animales = {"Alce", "Caballo", "Caracol", "C�ndor", "Gato", "Le�n", "Pit�n", "Tortuga"};
        
        int [][] matriz = {
            {1,1,0},
            {1,1,1},
            {1,0,0},
            {0,0,0},
            {0,1,1},
            {0,1,0},
            {0,0,1},
            {1,0,1}
        };
        
        
        Scanner s = new Scanner(System.in);
        System.out.println("A continuaci�n haremos 3 preguntas para determinar"
                + "el animal que ha elegido");
        
        System.out.println("El animal elegido es hervivoro? 1=si/0=No");
        int hervivoro = s.nextInt();
        
        System.out.println("El animal elegido es mamifero? 1=si/0=No");
        int mamifero = s.nextInt();
        
        System.out.println("El animal elegido es domestico? 1=si/0=No");
        int domestico = s.nextInt();
        
        // matriz de valores
        for(int i = 0; i<8 ; i++){
            if (hervivoro == matriz[i][0]&&
                mamifero == matriz[i][1]&&
                domestico == matriz[i][2]){
                //Si entra ac� es porque encontr� una coincidencia
                System.out.println("El animal elegido es: " + animales[i]);
            }
        }
    }
    
}

