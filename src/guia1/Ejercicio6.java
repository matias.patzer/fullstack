/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia1;

import java.util.Scanner;

/**
Pedir al usuario que ingrese el precio de un producto y el porcentaje
de descuento. A continuación mostrar por pantalla el importe
descontado y el importe a pagar.
 */
public class Ejercicio6 {
    public static void main(String[] args) {
        
        System.out.println("Ejercicio 6: Descuentos");
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el precio del producto= ");
        double precio = s.nextFloat();
        System.out.println("Ingrese el descuento(en porcentaje)= ");
        double descuento = s.nextFloat();
        precio = precio - precio*(descuento/100);
        System.out.println("El precio final es de: $"+precio);
        
    
    }
}
