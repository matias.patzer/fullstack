/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia2;

import java.util.Arrays;
import java.util.Scanner;

/**
Escribir un algoritmo que lea un valor n por teclado y muestre los
primeros n t�rminos de la sucesi�n de Fibonacci.
Tip: la sucesi�n de Fibonacci comienza con 0, luego 1 y a partir de all�
cada nuevo n�mero es la suma de los dos anteriores.
*/
public class Ejercicio5 {
    public static void main(String[] args) {
    System.out.println("Ejercicio 5: serie de fibonacci");
    System.out.println("Ingrese el n�mero de iteraciones: ");
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    int f = 0;
    int t1= 1;
    int t2;
    
    for (int i=1; i<=n; i++){
        t2 = f;
        f = t1 + f;
        t1 = t2;
        System.out.println(t1);
    }
    
    
    System.out.println("La serie de fibonacci hasta el numero: " + n);
    System.out.println("Resulta en: " + f);
    
    
    }
}
    
    
