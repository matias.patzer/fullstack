/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia3;

import java.util.Scanner;



public class Libro {
    
    // Declaracion de atributos
    private int isbn;
    private String titulo;
    private String autor;
    private int paginas;
            
    // constructores
    public Libro(){}
    
    public Libro(int isbn, String titulo, String autor, int paginas){
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
    }        
           
    // M�todos
    public void cargarLibro(){
        Scanner s = new Scanner(System.in);
        // aca el usuario ingresa los datos del libro
        System.out.println("Ingrese el ISBN: ");
        this.isbn = s.nextInt();
         System.out.println("Ingrese el T�tulo: ");
        this.titulo = s.next();
         System.out.println("Ingrese el autor: ");
        this.autor = s.next();
         System.out.println("Ingrese el numero de paginas: ");
        this.paginas = s.nextInt();
    }
    
    public String datosLibro(){
        return "ISBN: " + this.isbn + " ; T�tulo: " + this.titulo +
                " ; Autor: " + this.autor + " ; Paginas: " + this.paginas;
    }
    
    
}
