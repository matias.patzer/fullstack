/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia1;

import java.util.Scanner;

/**
 *
 * @author matia
 */
public class Ejercicio7 {
    public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    System.out.println("Ingrese edad de la persona 1: ");
    int edad1 = s.nextInt();
    System.out.println("Ingrese edad de la persona 2: ");
    int edad2 = s.nextInt();
    
    int aux;
    aux = edad1;
    edad1 = edad2;
    edad2 = aux;
    // ahora se intercambian las edades y se imprimen los valores
    System.out.println("Edad 1: " + edad1+ "; Edad 2: " + edad2);
    
    }
}
