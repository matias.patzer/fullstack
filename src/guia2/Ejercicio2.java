/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guia2;

import java.util.Arrays;
import java.util.Scanner;

/**
 Escribir un programa que lea una palabra por teclado y determine si
es un palíndromo.
Tip: los palíndromos son palabras que se leen igual de izquierda a
derecha y viceversa. Por ejemplo, reconocer.
 
 */
public class Ejercicio2 {
    public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    System.out.println("Ingrese una palabra para ver si es palíndromo: ");
    String txt = s.next();
    String[] array = txt.split("");
    //System.out.println(Arrays.toString(array));
    
    //hola
    
    System.out.println("La palabra ingresada es: " + txt);
    String[] palindro = array.clone();
    for (int i=0;i<array.length;i++){
        palindro[i] = array[array.length-1-i];
    }
    System.out.println("palindro:" + Arrays.toString(palindro));
    
    System.out.println("Es palíndromo? --> " + Arrays.equals(palindro, array));
    }
}
    
